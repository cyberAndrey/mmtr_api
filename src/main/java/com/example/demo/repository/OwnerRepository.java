package com.example.demo.repository;

import com.example.demo.model.OwnerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OwnerRepository extends JpaRepository<OwnerEntity, Long>, JpaSpecificationExecutor<OwnerEntity> {
    OwnerEntity save(OwnerEntity owner);
    Optional<OwnerEntity> findById(Long id);
    List<OwnerEntity> findAll();
    void deleteById(Long id);
}
