package com.example.demo.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Owner")
public class OwnerEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "propSeq")
    @SequenceGenerator(name = "propSeq", sequenceName = "property_sequence", allocationSize = 1)
    private Long id;

    @Column(name = "first_name")
    private String first_name;

    @Column(name = "last_name")
    private String last_name;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "email")
    private String email;

    @OneToMany
    private List<CarEntity> cars;
}
