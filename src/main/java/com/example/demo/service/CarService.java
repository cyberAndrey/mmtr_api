package com.example.demo.service;

import com.example.demo.model.CarEntity;
import com.example.demo.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CarService {

    @Autowired
    CarRepository carRepository;

    public Optional<CarEntity> getById(Long id) {
        return carRepository.findById(id);
    }

    public void save(CarEntity entity) {
        carRepository.save(entity);
    }

    public void delete(Long id) {
        carRepository.deleteById(id);
    }

    public List<CarEntity> getAll() {
        return carRepository.findAll();
    }
}
