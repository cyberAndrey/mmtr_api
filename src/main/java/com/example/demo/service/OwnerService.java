package com.example.demo.service;

import com.example.demo.model.OwnerEntity;
import com.example.demo.repository.OwnerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OwnerService {

    @Autowired
    OwnerRepository ownerRepository;

    public Optional<OwnerEntity> getById(Long id) {
        return ownerRepository.findById(id);
    }

    public void save(OwnerEntity entity) {
        ownerRepository.save(entity);
    }

    public void delete(Long id) {
        ownerRepository.deleteById(id);
    }

    public List<OwnerEntity> getAll() {
        return ownerRepository.findAll();
    }
}
